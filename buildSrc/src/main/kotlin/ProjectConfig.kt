object ProjectConfig {
    const val COMPILE_SDK = 28
    const val MIN_SDK = 21
    const val TARGET_SDK = 28
    const val VERSION_CODE = 1
    const val VERSION_NAME = "1.0"
    val supportedLangs = listOf("en")
}