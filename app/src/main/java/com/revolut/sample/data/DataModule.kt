package com.revolut.sample.data

import com.revolut.sample.screen.converter.CurrencyRatesConverter
import com.revolut.sample.screen.converter.RepositoryCurrencyRatesConverter
import dagger.Binds
import dagger.Module
import javax.inject.Singleton

@Module
abstract class DataModule {

    @Binds @Singleton
    abstract fun provideCurrencyRatesRepository(repository: RetrofitCurrencyRepository): CurrencyRatesRepository

    @Binds @Singleton
    abstract fun provideCurrencyInfo(localCurrencyInfo: GDriveCurrencyFlagResolver): CurrencyFlagResolver

    @Binds @Singleton
    abstract fun provideRatesConverter(converter: RepositoryCurrencyRatesConverter): CurrencyRatesConverter
}
