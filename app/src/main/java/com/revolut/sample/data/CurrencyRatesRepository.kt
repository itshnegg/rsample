package com.revolut.sample.data

import com.revolut.sample.api.CurrencyRatesApi
import com.revolut.sample.api.CurrencyRatesResponse
import io.reactivex.Single
import io.reactivex.schedulers.Schedulers
import org.joda.money.CurrencyUnit
import org.joda.money.IllegalCurrencyException
import timber.log.Timber
import javax.inject.Inject
import javax.inject.Singleton

private const val BASE_AMOUNT = 1.0

interface CurrencyRatesRepository {

    fun observeCurrencyRates(): Single<List<CurrencyRate>>

}

data class CurrencyRate(
    val currency: CurrencyUnit,
    val rate: Double
)

@Singleton
class RetrofitCurrencyRepository @Inject constructor(
    private val currencyRatesApi: CurrencyRatesApi
) : CurrencyRatesRepository {

    override fun observeCurrencyRates(): Single<List<CurrencyRate>> = currencyRatesApi
        .getRates()
        .subscribeOn(Schedulers.io())
        .observeOn(Schedulers.computation())
        .map { it.toRatesList() }
}

private fun CurrencyRatesResponse.toRatesList(): List<CurrencyRate> =
    listOf(base to BASE_AMOUNT)
        .plus(rates.map { it.key to it.value })
        .mapNotNull { (code, value) ->
            try {
                CurrencyRate(CurrencyUnit.of(code), value)
            } catch (e: IllegalCurrencyException) {
                Timber.w("Can't parse currency: $code")
                null
            }
        }

