package com.revolut.sample.data

import android.net.Uri
import androidx.core.net.toUri
import javax.inject.Inject
import javax.inject.Singleton

interface CurrencyFlagResolver {

    fun getCurrencyFlag(abbr: String): Uri?
}

@Singleton
class GDriveCurrencyFlagResolver @Inject constructor() : CurrencyFlagResolver {
    
    private val currencyLogos: Map<String, String> = mapOf(
        "EUR" to "1lAZl4ZcFLWKog65Ew6aV04mypeXzFd_h",
        "AUD" to "1AZTalnA-NTmSY8Xnbd4Ej7qFuB8VkGQZ",
        "BGN" to "1HAdhJAPENBVUiRrVgaKosmZxtZ9itCze",
        "BRL" to "1FCc7QRmCW718l94V-8eAu_dqZ1yG8kWo",
        "CAD" to "1i3PtTheaRFQoPJUWt5Hn_ThX9a6I75dt",
        "CHF" to "1akPBdUqiA6VzJRTIYE8FRJUesASeuhQ6",
        "CNY" to "1226f0nXAk6hRTKDvimABkM8xUDQLJkUf",
        "CZK" to "1IfthbQZi5RTkEhwcvtppDsNIbdtigS3T",
        "DKK" to "1to-6fae2aDRDSz8p9qeZiSuAnB40i8Iw",
        "GBP" to "1cW0T8fkjJwvDlDUKrJZo_akc3cdaurAH",
        "HKD" to "1K5Xb4qp0NbkLbq-6P-wiDaD-5igVVfHC",
        "HRK" to "1mGvFH3Oc4hTUIu4pS_YOnDm8aag-WwfV",
        "HUF" to "12DWwtNC43F-aOFiQg1Y114P8u6e9MPFH",
        "IDR" to "15Jy_msUv71R8Okt_whojI5Oz6VHde5S9",
        "ILS" to "1MriLcl2RfqWwQVU-NZpQj_gmBSbWcXF6",
        "INR" to "1srxiXsJ6zrRE2sKJXsvIzcF3UIGbQmi5",
        "ISK" to "1RVtbtqMnNrV6sfEWBhuBuest9Pq_vJhX",
        "JPY" to "1uM-DOceGo0pP7-FqxvNiKmaU87EISNz5",
        "KRW" to "1mfBJWgaFWPj97PMTiClCG84RtxrlRBnp",
        "MXN" to "12rGrbBq33jHCIYbMdZHZe8sTJbTTn60L",
        "MYR" to "14aTK02BGx1W8LWuzr7ncQ0u50Xmh-HeS",
        "NOK" to "1O5l-M7bncPRycPZtAvQIWhI_NEVRmX5K",
        "NZD" to "1gdent83WDU1LirQ31gEG97e_Tts5pp_i",
        "PHP" to "1CcXZ9bfdcX5-gK7-IlrD_fR0FuuYPPpt",
        "PLN" to "1s_lUz48bMMOZGp8f0ZcyFQnSo4uB9APW",
        "RON" to "1AJ-VrvBX8Ew9fvdq7tFNRnwb4LjYy4tn",
        "RUB" to "1We5WxuDooicl7QKJYP-JV5Q39x57ogV5",
        "SEK" to "1nGyJDrkH9upADlUjO8DDD0RtwCOHE9nr",
        "SGD" to "1GNrdVTc4ppr8UFW7WmWy4NQgYwYHTwn_",
        "THB" to "1ZQVGwBK7k47-DyYCVMp9E-rwiu6t4VS2",
        "TRY" to "16ZVn70qK5s7narRhTvzLsNIzqljTPLxb",
        "USD" to "1kzRAg3FuRM9QW_kMaCHvGoTsFj-GdayX",
        "ZAR" to "1xaQSfdV0WdppKTAIRQqphV8wzTiPIgbs"
    )

    override fun getCurrencyFlag(abbr: String): Uri? = currencyLogos[abbr]
        ?.let { "https://drive.google.com/uc?export=download&id=$it".toUri() }
}
