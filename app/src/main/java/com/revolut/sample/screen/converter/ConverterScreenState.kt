package com.revolut.sample.screen.converter

import android.net.Uri
import com.revolut.sample.core.state.Action
import com.revolut.sample.core.state.State

data class ConverterScreenState(
    val loadingIndicatorVisible: Boolean = false,
    val currencies: List<CurrencyConverterEntry> = emptyList()
) : State

data class CurrencyConverterEntry(
    val imageUri: Uri? = null,
    val abbreviation: String,
    val fullName: String,
    val amount: String?,
    val active: Boolean
)

sealed class ConverterScreenAction : Action {

    object LoadCurrencies : ConverterScreenAction()
    data class ShowCurrencies(val currencies: List<CurrencyConverterEntry>) : ConverterScreenAction()
    data class ChangeRatesBase(val baseCurrency: CurrencyConverterEntry) : ConverterScreenAction()
}
