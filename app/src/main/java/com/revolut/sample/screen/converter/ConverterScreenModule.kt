package com.revolut.sample.screen.converter

import com.revolut.sample.core.di.ActivityScope
import com.revolut.sample.core.viewmodel.LifecycleManager
import dagger.Module
import dagger.Provides

@Module
class ConverterScreenModule {

    @Provides @ActivityScope
    fun providesLifecycleManager(activity: ConverterScreenActivity): LifecycleManager =
        LifecycleManager(activity.lifecycle)
}
