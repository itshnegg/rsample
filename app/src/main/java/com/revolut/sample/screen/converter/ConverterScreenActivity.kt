package com.revolut.sample.screen.converter

import android.os.Bundle
import android.support.v7.util.DiffUtil
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import androidx.core.view.isVisible
import com.revolut.sample.R
import com.revolut.sample.core.activity.BaseActivity
import com.revolut.sample.ext.hideKeyboard
import com.revolut.sample.recyclerview.ListDiffCallback
import kotlinx.android.synthetic.main.screen_converter.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.async
import kotlinx.coroutines.launch

private const val DEFAULT_VIEW_TYPE = 0
private const val MAX_VIEW_HOLDERS = 20

class ConverterScreenActivity : BaseActivity<ConverterScreenState, ConverterScreenAction>() {
    override val viewModel by lazy { getViewModel(ConverterScreenViewModel::class) }
    private val converterScreenAdapter: ConverterScreenAdapter
        get() = screen_converter_recycler_view.adapter as ConverterScreenAdapter
    private var listUpdateJob: Job? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.screen_converter)
    }

    override fun render(newState: ConverterScreenState, prevState: ConverterScreenState?) {
        renderLoadingIndicator(newState, prevState)
        renderCurrenciesList(newState, prevState)
    }

    private fun renderCurrenciesList(newState: ConverterScreenState, prevState: ConverterScreenState?) {
        if (prevState == null) {
            with(screen_converter_recycler_view) {
                layoutManager = LinearLayoutManager(this@ConverterScreenActivity)
                setHasFixedSize(true)
                recycledViewPool.setMaxRecycledViews(DEFAULT_VIEW_TYPE, MAX_VIEW_HOLDERS)
                adapter = ConverterScreenAdapter { viewModel.dispatch(ConverterScreenAction.ChangeRatesBase(it)) }
                addOnScrollListener(object : RecyclerView.OnScrollListener() {
                    override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
                        if (newState == RecyclerView.SCROLL_STATE_DRAGGING) hideKeyboard()
                    }
                })
            }
        }
        when {
            newState.currencies.isEmpty() -> viewModel.dispatch(ConverterScreenAction.LoadCurrencies)

            newState.currencies != prevState?.currencies -> {
                val diffUtilCallback = ListDiffCallback(
                    prevState?.currencies ?: emptyList(),
                    newState.currencies,
                    itemsTheSame = { old, new -> old.abbreviation == new.abbreviation },
                    getChangePayload = { _, new -> ConverterScreenAdapterPayload(new.amount, new.active) }
                )
                if (listUpdateJob?.isActive == true) listUpdateJob?.cancel()
                listUpdateJob = launch(Dispatchers.Main) {
                    async(Dispatchers.Default) { DiffUtil.calculateDiff(diffUtilCallback) }
                        .await()
                        .run {
                            converterScreenAdapter.items = newState.currencies
                            dispatchUpdatesTo(converterScreenAdapter)
                            val newBaseCurrency = newState.currencies[0].abbreviation !=
                                    prevState?.currencies?.getOrNull(0)?.abbreviation
                            if (newBaseCurrency) screen_converter_recycler_view.scrollToPosition(0)
                        }
                }
            }
        }
    }

    private fun renderLoadingIndicator(newState: ConverterScreenState, prevState: ConverterScreenState?) {
        if (newState.loadingIndicatorVisible != prevState?.loadingIndicatorVisible) {
            screen_converter_loading_indicator.isVisible = newState.loadingIndicatorVisible
        }
    }
}
