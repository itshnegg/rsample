package com.revolut.sample.screen.converter

import com.revolut.sample.core.viewmodel.BaseViewModel
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.disposables.Disposables
import timber.log.Timber
import java.math.BigDecimal
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class ConverterScreenViewModel @Inject constructor(
    private val converter: CurrencyRatesConverter
) : BaseViewModel<ConverterScreenState, ConverterScreenAction>(ConverterScreenState()) {

    private var ratesDisposable: Disposable = Disposables.disposed()

    override fun reduce(previousState: ConverterScreenState, action: ConverterScreenAction): ConverterScreenState {
        return when (action) {
            is ConverterScreenAction.LoadCurrencies -> observeRates(previousState)
            is ConverterScreenAction.ShowCurrencies -> showCurrencies(previousState, action)
            is ConverterScreenAction.ChangeRatesBase -> changeRatesBase(previousState, action)
        }
    }

    private fun observeRates(previousState: ConverterScreenState): ConverterScreenState {
        if (ratesDisposable.isDisposed) {
            ratesDisposable = converter
                .getCurrencyRates()
                .compose(subscribeWhileObserved())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                    { dispatch(ConverterScreenAction.ShowCurrencies(it)) },
                    { Timber.e(it) }
                )
        }
        return previousState.copy(loadingIndicatorVisible = true)
    }

    private fun showCurrencies(
        previousState: ConverterScreenState,
        action: ConverterScreenAction.ShowCurrencies
    ) = previousState.copy(currencies = action.currencies, loadingIndicatorVisible = false)

    private fun changeRatesBase(
        previousState: ConverterScreenState,
        action: ConverterScreenAction.ChangeRatesBase
    ): ConverterScreenState {
        converter.changeBase(
            CurrencyRatesBase(
                action.baseCurrency.abbreviation,
                action.baseCurrency.amount?.toBigDecimalOrNull() ?: BigDecimal(0)
            )
        )
        return previousState
    }

    override fun onCleared() {
        super.onCleared()
        ratesDisposable.dispose()
    }
}
