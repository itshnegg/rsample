package com.revolut.sample.screen.converter

import android.support.v7.widget.RecyclerView
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.EditorInfo
import android.widget.EditText
import android.widget.ImageView
import android.widget.TextView
import com.revolut.sample.R
import com.revolut.sample.ext.hideKeyboard
import com.revolut.sample.ext.showKeyboard
import com.revolut.sample.glide.GlideApp
import org.jetbrains.anko.dimen

class ConverterScreenAdapter(
    private val changeListener: (CurrencyConverterEntry) -> Unit
) : RecyclerView.Adapter<ConverterScreenAdapter.CurrencyEntryViewHolder>() {
    var items: List<CurrencyConverterEntry> = emptyList()
    private var textWatcher: TextWatcher? = null

    override fun onCreateViewHolder(vg: ViewGroup, viewType: Int): CurrencyEntryViewHolder =
        CurrencyEntryViewHolder.fromViewGroup(vg)

    override fun getItemCount(): Int = items.size

    override fun onBindViewHolder(viewHolder: CurrencyEntryViewHolder, position: Int) {
        val data = items[position]
        with(viewHolder) {
            val flagSize = itemView.context.dimen(R.dimen.flag_size)
            GlideApp
                .with(viewHolder.countryIV)
                .load(data.imageUri)
                .placeholder(R.drawable.ic_currecny_placeholder)
                .error(R.drawable.ic_currecny_placeholder)
                .circleCrop()
                .override(flagSize, flagSize)
                .into(countryIV)
            abbreviationTV.text = data.abbreviation
            countryNameTV.text = data.fullName
            with(valueET) {
                setText(data.amount)
                setOnEditorActionListener { _, actionId, _ ->
                    (actionId == EditorInfo.IME_ACTION_DONE).also { done ->
                        if (done) {
                            clearFocus()
                            hideKeyboard()
                        }
                    }
                }
            }
            if (data.active) setupActiveInput(data) else setupPassiveInput(data)
        }
    }

    override fun onBindViewHolder(holder: CurrencyEntryViewHolder, position: Int, payloads: MutableList<Any>) {
        payloads
            .getOrNull(0)
            ?.run {
                val payload = this as ConverterScreenAdapterPayload
                if (payload.active) {
                    holder.valueET.prepareInput()
                    holder.setupActiveInput(items[position])
                } else {
                    holder.setupPassiveInput(items[position])
                    holder.valueET.setText(payload.newAmount)
                }
            }
            ?: super.onBindViewHolder(holder, position, payloads)
    }

    private fun CurrencyEntryViewHolder.setupActiveInput(data: CurrencyConverterEntry) {
        valueET.onFocusChangeListener = null
        if (textWatcher == null) {
            textWatcher = object : TextWatcher {
                override fun afterTextChanged(editable: Editable) {
                    if (valueET.isFocused) changeListener(data.copy(amount = editable.toString()))
                }

                override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) = Unit
                override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) = Unit
            }
            valueET.addTextChangedListener(textWatcher)
        }
        itemView.setOnClickListener { valueET.prepareInput() }
    }

    private fun CurrencyEntryViewHolder.setupPassiveInput(data: CurrencyConverterEntry) {
        if (textWatcher != null) {
            valueET.removeTextChangedListener(textWatcher)
            textWatcher = null
        }
        valueET.setOnFocusChangeListener { _, focus ->
            if (focus) changeListener(data.copy(amount = valueET.text.toString()))
        }
        itemView.setOnClickListener { changeListener(data.copy(amount = valueET.text.toString())) }
    }

    private fun EditText.prepareInput() {
        requestFocus()
        setSelection(length())
        showKeyboard()
    }

    class CurrencyEntryViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        val countryIV: ImageView = itemView.findViewById(R.id.item_currency_image)
        val abbreviationTV: TextView = itemView.findViewById(R.id.item_currency_abbr)
        val countryNameTV: TextView = itemView.findViewById(R.id.item_currency_full_name)
        val valueET: EditText = itemView.findViewById(R.id.item_currency_value)

        companion object {

            fun fromViewGroup(viewGroup: ViewGroup): CurrencyEntryViewHolder =
                LayoutInflater
                    .from(viewGroup.context)
                    .inflate(R.layout.item_currency, viewGroup, false)
                    .let { CurrencyEntryViewHolder(it) }
        }
    }
}

data class ConverterScreenAdapterPayload(val newAmount: String?, val active: Boolean)
