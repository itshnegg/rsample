package com.revolut.sample.screen.converter

import com.revolut.sample.data.CurrencyFlagResolver
import com.revolut.sample.data.CurrencyRate
import com.revolut.sample.data.CurrencyRatesRepository
import io.reactivex.Observable
import io.reactivex.rxkotlin.Observables
import io.reactivex.schedulers.Schedulers
import io.reactivex.subjects.BehaviorSubject
import io.reactivex.subjects.Subject
import org.joda.money.Money
import timber.log.Timber
import java.math.BigDecimal
import java.math.RoundingMode
import java.util.*
import java.util.concurrent.TimeUnit
import java.util.concurrent.atomic.AtomicLong
import javax.inject.Inject
import kotlin.math.min

private const val INITIAL_DELAY_SEC = 0L
private const val REFRESH_DELAY_SEC = 1L
private const val INITIAL_DELAY_ON_FAIL_SEC = 1L
private const val MAX_DELAY_ON_FAIL_SEC = 5L
private const val DELAY_STEP_SEC = 1L
private const val DEFAULT_MONEY_AMOUNT = 1
private const val USER_INPUT_DEBOUNCE_MS = 50L
private const val DEFAULT_AMOUNT_STRING = "0"

interface CurrencyRatesConverter {

    fun getCurrencyRates(): Observable<List<CurrencyConverterEntry>>

    fun changeBase(base: CurrencyRatesBase)
}

class RepositoryCurrencyRatesConverter @Inject constructor(
    private val repository: CurrencyRatesRepository,
    private val flagResolver: CurrencyFlagResolver
) : CurrencyRatesConverter {

    private val baseCurrencySubject: Subject<CurrencyRatesBase> =
        BehaviorSubject
            .createDefault(CurrencyRatesBase(null))
            .toSerialized()
    private var failedDelay: AtomicLong = AtomicLong(INITIAL_DELAY_ON_FAIL_SEC)

    override fun getCurrencyRates(): Observable<List<CurrencyConverterEntry>> {
        val ratesObservable: Observable<List<CurrencyRate>> =
            Observable.interval(INITIAL_DELAY_SEC, REFRESH_DELAY_SEC, TimeUnit.SECONDS)
                .subscribeOn(Schedulers.io())
                .observeOn(Schedulers.computation())
                .switchMap { repository.observeCurrencyRates().toObservable() }
                .filter { it.isNotEmpty() }
                .doOnNext { failedDelay.set(INITIAL_DELAY_ON_FAIL_SEC) }
                .retryWhen { observable ->
                    observable.switchMap {
                        val nextDelay = min(failedDelay.get() + DELAY_STEP_SEC, MAX_DELAY_ON_FAIL_SEC)
                        Observable.just(Unit)
                            .delay(failedDelay.getAndSet(nextDelay), TimeUnit.SECONDS)
                    }

                }

        val baseRatesObservable: Observable<CurrencyRatesBase> =
            baseCurrencySubject
                .hide()
                .observeOn(Schedulers.computation())
                .debounce(USER_INPUT_DEBOUNCE_MS, TimeUnit.MILLISECONDS)
                .distinctUntilChanged()

        return Observables.combineLatest(
            ratesObservable,
            baseRatesObservable
        ) { rates, (code, amount) ->

            val userMoney = code
                ?.let {currency ->
                    val roundedAmount = amount.setScale(2, RoundingMode.HALF_DOWN).toPlainString()
                    "$currency $roundedAmount".toMoneyOrNull()
                }
                ?: "${rates[0].currency} $DEFAULT_MONEY_AMOUNT".toMoneyOrNull()

            val baseRate = rates.firstOrNull { it.currency == userMoney?.currencyUnit } ?: rates[0]
            if (userMoney == null) {
                Timber.w("Invalid base currencies ${code to amount} & ${rates[0].currency to DEFAULT_MONEY_AMOUNT}")
                emptyList()
            } else {
                rates
                    .map { it.toCurrencyConverterEntry(userMoney, baseRate) }
                    .sortedBy { it.abbreviation != code }
            }
        }
    }

    override fun changeBase(base: CurrencyRatesBase) = baseCurrencySubject.onNext(base)

    private fun CurrencyRate.toCurrencyConverterEntry(money: Money, baseRate: CurrencyRate): CurrencyConverterEntry {
        val baseCurrency = currency == money.currencyUnit
        val convertedAmount = if (baseCurrency) {
            money.amount
        } else {
            money.convertedTo(
                currency,
                (rate / baseRate.rate).toBigDecimal(),
                RoundingMode.DOWN
            ).amount
        }
        val amountString = when {
            convertedAmount.notZero() -> convertedAmount.stripTrailingZeros().toPlainString()
            money.amount.notZero() -> DEFAULT_AMOUNT_STRING
            else -> null
        }
        return CurrencyConverterEntry(
            flagResolver.getCurrencyFlag(currency.code),
            currency.code,
            currency.toCurrency().getDisplayName(Locale.ENGLISH),
            amountString,
            baseCurrency
        )
    }
}

data class CurrencyRatesBase(val code: String?, val amount: BigDecimal = BigDecimal.ZERO)

private fun String.toMoneyOrNull(): Money? {
    return try {
        Money.parse(this)
    } catch (e: Exception) {
        when (e) {
            is IllegalArgumentException, is ArithmeticException -> {
                Timber.e(e)
                null
            }
            else -> throw e
        }
    }
}

private fun BigDecimal.notZero() = compareTo(BigDecimal.ZERO) != 0
