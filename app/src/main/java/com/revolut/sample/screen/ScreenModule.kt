package com.revolut.sample.screen

import com.revolut.sample.core.di.ActivityScope
import com.revolut.sample.screen.converter.ConverterScreenActivity
import com.revolut.sample.screen.converter.ConverterScreenModule
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class ScreenModule {

    @ContributesAndroidInjector(modules = [ConverterScreenModule::class]) @ActivityScope
    abstract fun contributeConverterActivity(): ConverterScreenActivity
}
