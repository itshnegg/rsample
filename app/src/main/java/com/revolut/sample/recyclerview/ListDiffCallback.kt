package com.revolut.sample.recyclerview

import android.support.v7.util.DiffUtil

class ListDiffCallback<out T: Any>(
    private val oldList: List<T>,
    private val newList: List<T>,
    private val itemsTheSame: (T, T) -> Boolean = { old, new -> old == new },
    private val contentsTheSame: (T, T) -> Boolean = { old, new -> old == new },
    private val getChangePayload: (T, T) -> Any? = { _, _ -> null}
) : DiffUtil.Callback() {

    override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean =
            itemsTheSame(oldList[oldItemPosition], newList[newItemPosition])

    override fun getOldListSize(): Int = oldList.size

    override fun getNewListSize(): Int = newList.size

    override fun areContentsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean =
            contentsTheSame(oldList[oldItemPosition], newList[newItemPosition])

    override fun getChangePayload(oldItemPosition: Int, newItemPosition: Int): Any? =
            getChangePayload(oldList[oldItemPosition], newList[newItemPosition])
}
