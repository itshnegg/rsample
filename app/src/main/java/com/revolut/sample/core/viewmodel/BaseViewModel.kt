package com.revolut.sample.core.viewmodel

import android.arch.lifecycle.ViewModel
import android.support.annotation.CallSuper
import com.revolut.sample.core.state.*
import io.reactivex.Observable
import io.reactivex.ObservableTransformer
import io.reactivex.subjects.BehaviorSubject
import io.reactivex.subjects.Subject
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlin.coroutines.CoroutineContext

abstract class BaseViewModel<T : State, V : Action>(
    private val initialState: T
) : ViewModel(),
    Dispatcher<V>,
    Reducer<T, V>,
    StateEmitter<T>,
    CoroutineScope {

    private val stateSubject: Subject<T> = BehaviorSubject.createDefault(initialState).toSerialized()
    private val subscriptionSubject = BehaviorSubject.createDefault<Boolean>(false).toSerialized()
    private var currentState: T = initialState
    private val viewModelJob = Job()
    override val coroutineContext: CoroutineContext
        get() = Dispatchers.Default + viewModelJob

    override fun observeState(): Observable<T> = stateSubject
        .distinctUntilChanged()
        .doOnSubscribe { subscriptionSubject.onNext(true) }
        .doOnDispose { subscriptionSubject.onNext(false) }
        .doOnNext { newState -> currentState = newState }

    override fun dispatch(action: V) = stateSubject.onNext(reduce(currentState, action))

    protected fun <T> subscribeWhileObserved() =
        ObservableTransformer<T, T> { observable ->
            subscriptionSubject
                .distinctUntilChanged()
                .switchMap { subscribed ->
                    if (subscribed) observable else Observable.never()
                }
        }

    @CallSuper
    override fun onCleared() {
        viewModelJob.cancel()
        stateSubject.onNext(initialState)
    }
}
