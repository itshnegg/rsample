package com.revolut.sample.core.viewmodel

import android.arch.lifecycle.Lifecycle
import android.arch.lifecycle.LifecycleObserver
import android.arch.lifecycle.LifecycleOwner
import android.arch.lifecycle.OnLifecycleEvent
import io.reactivex.Observable
import io.reactivex.ObservableTransformer
import io.reactivex.subjects.BehaviorSubject

class LifecycleManager(private val lifecycle: Lifecycle) {

    private val stateSubject = BehaviorSubject.createDefault<Lifecycle.State>(lifecycle.currentState)
    private val currentState
        get() = lifecycle.currentState

    init {
        lifecycle.addObserver(object : LifecycleObserver {
            @OnLifecycleEvent(Lifecycle.Event.ON_ANY)
            fun onEvent(source: LifecycleOwner, event: Lifecycle.Event) = stateSubject.onNext(currentState)
        })
    }

    fun <T> subscribeWhile(state: Lifecycle.State) =
        ObservableTransformer<T, T> { observable ->
            stateSubject
                .map { acceptedState -> acceptedState.isAtLeast(state) }
                .distinctUntilChanged()
                .switchMap { changed ->
                    if (changed) observable else Observable.never()
                }
        }
}
