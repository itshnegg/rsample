package com.revolut.sample.core.di

import javax.inject.Scope

@Scope @Retention(AnnotationRetention.RUNTIME)
annotation class ActivityScope
