package com.revolut.sample.core.viewmodel

import android.arch.lifecycle.ViewModel
import dagger.MapKey
import kotlin.reflect.KClass

@Retention(AnnotationRetention.RUNTIME) @MapKey
internal annotation class ViewModelKey(val value: KClass<out ViewModel>)
