package com.revolut.sample.core.state

interface Dispatcher<T : Action> {

    fun dispatch(action: T)
}
