package com.revolut.sample.core.viewmodel

import android.arch.lifecycle.ViewModel
import android.arch.lifecycle.ViewModelProvider
import com.revolut.sample.screen.converter.ConverterScreenViewModel
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap
import javax.inject.Singleton

@Module
abstract class ViewModelModule {

    @Binds @Singleton @IntoMap @ViewModelKey(ConverterScreenViewModel::class)
    abstract fun bindGraphViewModel(viewModel: ConverterScreenViewModel): ViewModel

    @Binds @Singleton
    abstract fun bindViewModelFactory(factory: ViewModelFactory): ViewModelProvider.Factory
}
