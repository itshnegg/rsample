package com.revolut.sample.core.activity

import android.arch.lifecycle.Lifecycle
import android.arch.lifecycle.ViewModelProvider
import android.arch.lifecycle.ViewModelProviders
import android.os.Bundle
import android.support.annotation.CallSuper
import android.support.v7.app.AppCompatActivity
import com.revolut.sample.core.di.Injectable
import com.revolut.sample.core.state.Action
import com.revolut.sample.core.state.Renderer
import com.revolut.sample.core.state.State
import com.revolut.sample.core.viewmodel.BaseViewModel
import com.revolut.sample.core.viewmodel.LifecycleManager
import io.reactivex.BackpressureStrategy
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import timber.log.Timber
import javax.inject.Inject
import kotlin.coroutines.CoroutineContext
import kotlin.reflect.KClass

abstract class BaseActivity<T : State, A: Action> : AppCompatActivity(), Injectable, Renderer<T>, CoroutineScope {

    @set:Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory
    @set:Inject
    lateinit var lifecycleManager: LifecycleManager
    private var currentState: T? = null
    private lateinit var stateDisposable: Disposable
    abstract val viewModel: BaseViewModel<T, A>
    private lateinit var job: Job
    override val coroutineContext: CoroutineContext
        get() = Dispatchers.Main + job

    protected fun <T : BaseViewModel<*, *>> getViewModel(modelClass: KClass<T>): T =
        ViewModelProviders.of(this, viewModelFactory).get(modelClass.java)

    @CallSuper
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        job = Job()
        observeState()
    }

    private fun observeState() {
        stateDisposable = viewModel
            .observeState()
            .compose(lifecycleManager.subscribeWhile(Lifecycle.State.STARTED))
            .toFlowable(BackpressureStrategy.BUFFER)
            .distinctUntilChanged()
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(
                { newState ->
                    render(newState, currentState)
                    currentState = newState
                },
                { Timber.e(it) }
            )
    }

    override fun onDestroy() {
        super.onDestroy()
        job.cancel()
        stateDisposable.dispose()
    }
}
