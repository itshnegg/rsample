package com.revolut.sample.core.state

interface Reducer<S: State, in A: Action> {

    fun reduce(previousState: S, action: A): S = previousState
}
