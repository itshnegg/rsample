package com.revolut.sample.core.state

interface Renderer<in T : State> {

    fun render(newState: T, prevState: T?)
}
