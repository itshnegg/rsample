package com.revolut.sample.core.state

import io.reactivex.Observable

interface StateEmitter<T: State> {

    fun observeState(): Observable<T>
}
