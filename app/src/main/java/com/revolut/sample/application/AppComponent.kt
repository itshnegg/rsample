package com.revolut.sample.application

import android.content.Context
import com.revolut.sample.api.ApiModule
import com.revolut.sample.data.DataModule
import com.revolut.sample.screen.ScreenModule
import dagger.BindsInstance
import dagger.Component
import dagger.android.support.AndroidSupportInjectionModule
import javax.inject.Singleton


@Singleton
@Component(modules = [
    AndroidSupportInjectionModule::class,
    AppModule::class,
    ApiModule::class,
    DataModule::class,
    ScreenModule::class
])
interface AppComponent {

    companion object {

        fun init(context: Context): AppComponent =
                DaggerAppComponent.builder()
                        .context(context.applicationContext)
                        .build()
    }

    @Component.Builder
    interface Builder {

        @BindsInstance
        fun context(context: Context): Builder

        fun build(): AppComponent
    }

    fun inject(application: App)
}
