package com.revolut.sample.application

import android.content.Context
import android.content.res.Resources
import com.revolut.sample.core.viewmodel.ViewModelModule
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module(includes = [ViewModelModule::class])
class AppModule {

    @Provides @Singleton
    fun provideResources(context: Context): Resources = context.resources

}
