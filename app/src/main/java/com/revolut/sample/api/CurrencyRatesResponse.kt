package com.revolut.sample.api

import com.squareup.moshi.JsonClass
import io.reactivex.Single
import retrofit2.http.GET

interface CurrencyRatesApi {

    @GET("latest")
    fun getRates(): Single<CurrencyRatesResponse>
}

@JsonClass(generateAdapter = true)
data class CurrencyRatesResponse(
    val base: String,
    val date: String,
    val rates: Map<String, Double>
)
